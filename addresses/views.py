from django.urls import reverse_lazy
from django.views.generic import FormView
from .forms import AddressForm


class AddressFormView(FormView):
    form_class = AddressForm
    success_url = reverse_lazy('addresses:address')
    template_name = 'addresses/address.html'
