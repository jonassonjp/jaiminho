from django.conf.urls import url

from . import views

app_name='addresses'
urlpatterns = [
    url(r'^$', views.AddressFormView.as_view(), name='address')
]